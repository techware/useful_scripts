#!/bin/bash
./getip.sh
echo "Please edit the DNS record to connect the domain to this server."
echo "If you are ready type your new domain name."
read domain
# optain certificate
sudo certbot certonly --nginx -d $domain
# prepare website folder
sudo mkdir /var/www/$domain
sudo chown $USER:www-data -R /var/www/$domain
# setup nginx
sudo cp $PWD/templates/nginx.site.default /etc/nginx/sites-available/$domain
sudo sed -i "s/domain/${domain}/"  /etc/nginx/sites-available/$domain
sudo ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/$domain
sudo systemctl reload nginx.service

